'use strict';

/**
 * error that will be thrown in case a policy handler returns anything besides the options available via the RequestPolicy
 *
 * @class InvalidPolicyError
 * @extends {Error}
 */
class InvalidPolicyError extends Error {
  constructor(...params) {
    super(...params);
    Error.captureStackTrace(this, InvalidPolicyError);
    this.name = 'InvalidPolicyError';
  }
}

module.exports = InvalidPolicyError;
