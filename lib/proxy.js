'use strict';

const got = require('got');
const URL = require('url');
const HttpAgent = require('agentkeepalive');
const { HttpsAgent } = HttpAgent;
const RequestPolicy = require('./RequestPolicy');
const InvalidPolicyError = require('./InvalidPolicyError');

const defaultHttpsAgent = new HttpsAgent();
const defaultHttpAgent = new HttpAgent();

/**
 * @typedef {import('express').RequestHandler} RequestHandler
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 * @typedef {import('got').ReturnStream} GotReturnStream
 */

/**
 * @typedef OnAfterReadHandler
 * @param {Object} param
 * @param {Request} param.req the downstream client request
 * @param {Response} param.res the downstream client response
 * @param {NextFunction} param.next function to execute the next middleware
 * @param {GotReturnStream} param.targetResponse the upstream response object
 * @returns {RequestPolicy}
 */

/**
 * @typedef OnBeforeWriteHandler
 * @param {Object} param
 * @param {Request} param.req the downstream client request
 * @param {Response} param.res the downstream client response
 * @param {NextFunction} param.next function to execute the next middleware
 * @returns {RequestPolicy}
 */

/**
 * Callback Error Handler
 * @typedef ErrorHandler
 * @param {Object} param
 * @param {Request} param.req the downstream client request
 * @param {Response} param.res the downstream client response
 * @param {NextFunction} param.next function to execute the next middleware
 */

/** default handler for onAfterReads - will return not implemented error by default
 * @type {OnAfterReadHandler}
 */
const onAfterReadDefaultHandler = ({ req, res, next, targetResponse }) => {
  res.boom ? res.boom.notImplemented() : res.status(501).send();
  return RequestPolicy.handled;
};

/**
 * default handler for onBeforeWrite - will return not implemented error by default
 * @type {OnBeforeWriteHandler}
 */
const onBeforeWriteDefaultHandler = ({ req, res, next }) => {
  res.boom ? res.boom.notImplemented() : res.status(501).send();
  return RequestPolicy.handled;
};

/**
 * proxy express middleware
 *
 * @param {Object} param configuration options
 * @param {string} param.target the target hostname (url)
 * @param {bool} param.keepOriginalHostname set to true if the hostname header should be rewritten to the name of the target hostname (defaults to false)
 * @param {OnAfterReadHandler} param.onAfterRead event handler that will be called for READ requests after a response has been received by the upstream service
 * @param {OnBeforeWriteHandler} param.onBeforeWriteDefaultHandler event handler that will be called for every WRITE request against an upstream service
 * @param {ErrorHandler} param.forbiddenHandler handler to be called for requests that should be forbidden
 * @param {ErrorHandler} param.unsupportedOperation handler to be called for http methods that are not supported (e.g. TRACE method)
 * @throws {InvalidPolicyError} throws an invalid policy error in case the OnAfterReadHandler or OnBeforeWriteHandler handlers return anything but an RequestPolicy option.
 */
module.exports = ({
  target,
  keepOriginalHostname = false,
  onAfterRead = onAfterReadDefaultHandler,
  onBeforeWrite = onBeforeWriteDefaultHandler,
  forbiddenHandler = ({ req, res, next }) => (res.boom ? res.boom.forbidden() : res.status(403).send()),
  unsupportedOperation = ({ req, res, next }) => (res.boom ? res.boom.notImplemented() : res.status(501).send()),
}) => async (req, res, next) => {
  /**
   * forward the request to the upstream server
   */
  const forwardToUpstream = () => {
    // copy the original request headers over and replace the hostname with the
    // target url hostname in case keepOriginalHostname is set to false (default)
    const headers = Object.assign({}, req.headers, {
      host: keepOriginalHostname ? req.hostname : URL.parse(target).hostname,
    });

    if (typeof req.trace === 'string' && typeof req.traceHeader === 'string') {
      headers[req.traceHeader] = req.trace;
    }

    // set forwarding headers on upstream request
    const forwardHeader = headers['x-forwarded-for'];
    headers['x-forwarded-for'] = `${forwardHeader ? forwardHeader.concat(', ') : ''}${req.ip}`;

    // the default function will append the query string from the original request
    // to the target url
    const url = `${target}${req.originalUrl}`;

    // request parameters for got - our connection to the upstream server
    const options = {
      method: req.method,
      headers,
      url,
      agent: {
        http: defaultHttpAgent,
        https: defaultHttpsAgent,
      },
      throwHttpErrors: false,
    };
    // TODO: check if we would profit from using `Stream.pipeline(from, ..., to, callback)` here
    return req.pipe(got.stream(options));
  };

  /**
   * copy all data from the upstream service to the express response
   *
   * @param {Object} param
   * @param {Request} param.res express response object
   * @param {Object} param.targetResponse resolved http response stream (all data available) of the upstream connection
   * @param {GotReturnStream} param.upstream the upstream connection
   */
  const copyToResponse = ({ res, targetResponse, upstream }) => {
    // copy headers to response
    // express will take care to correct the content-size header
    // TODO: include our trace header in trace header from upstream
    Object.entries(targetResponse.headers).forEach(([name, value]) => res.set(name, value));
    // copy status code to response
    res.statusCode = targetResponse.statusCode;
    // pipe the body
    // TODO: check if we would profit from using `Stream.pipeline(from, ..., to, callback)` here
    upstream.pipe(res);
  };

  // differentiate between writes and reads here.
  // writes have to be checked before forwarding them to the target service
  // reads on their way back to the client.
  switch (req.method) {
    // check the response from the service
    case 'GET':
    case 'HEAD':
    case 'TRACE':
    case 'OPTIONS': {
      // forward the request to the upstream service
      const upstream = forwardToUpstream();
      // report bad gateway for connection failures/connection drops
      upstream.on('error', (err) => {
        res.boom ? res.boom.badGateway() : res.status(502).send();
      });
      // hand the response to our callback handler
      upstream.on('response', async (targetResponse) => {
        // decide on the outcome
        const policy = await onAfterRead({
          req,
          res,
          next,
          targetResponse,
        });

        // decide on what to do with the request
        switch (policy) {
          // forward the request vanilla to the client
          case RequestPolicy.forward:
            copyToResponse({ res, targetResponse, upstream });
            break;

          // the callback handler will have taken care of the request
          // (or) an upstream handler is supposed to
          case RequestPolicy.handled:
            // nothing for us to do -
            // TODO: maybe drop the targetResponse now and call next() here?
            break;

          // the request should be blocked
          case RequestPolicy.block:
            forbiddenHandler({ req, res, next });
            return;

          default:
            // unknown policy
            // TODO: log error since this is most likely a developer error
            throw new InvalidPolicyError();
        }
      });
      return;
    }

    // check the operation and/or payload before sending on to the service
    case 'POST':
    case 'PUT':
    case 'PATCH':
    case 'DELETE': {
      // handle writes

      // check the payload before sending the request to the target server
      const policy = await onBeforeWrite({ req, res, next });

      switch (policy) {
        // forward the request vanilla to the upstream server
        case RequestPolicy.forward:
          // we could now additionally verify that the server response doesn't contain any
          // information we don't want to expose to the user - no use case for now
          // TODO: catch connection errors
          const targetResponse = forwardToUpstream();
          // TODO: check if we would profit from using `Stream.pipeline(from, ..., to, callback)` here
          targetResponse.pipe(res);
          return;

        // the callback handler will have taken care of the request
        // (or) an upstream handler is supposed to
        case RequestPolicy.handled:
          // nothing for us to do
          break;

        // the request should be blocked
        case RequestPolicy.block:
          forbiddenHandler({ req, res, next });
          return;

        default:
          // unknown policy
          // TODO: log error since this is most likely a developer error
          throw new InvalidPolicyError();
      }
      return;
    }

    default:
      // don't handle at all
      // TODO: check for all these cases, that the correct headers are returned
      // mime-type, content-length, ...
      unsupportedOperation({ req, res, next });
      return;
  }
};
