'use strict';

/**
 * @typedef {Object} RequestPolicy
 * @property {string} forward will forward the response (via pipeline) from the target the the requestor without changing it
 * @property {string} block will abort the request and return a 403 forbidden error response to the client
 * @property {string} handled use this in case the response has been handled by the callback handler
 */
module.exports = Object.freeze({
  forward: 'forward',
  block: 'block',
  handled: 'handled'
});
